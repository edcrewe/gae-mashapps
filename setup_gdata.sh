export VERSION=2.0.14
wget http://gdata-python-client.googlecode.com/files/gdata-$VERSION.tar.gz
tar -zxvf gdata-$VERSION.tar.gz
mv gdata-$VERSION/src/atom atom
mv gdata-$VERSION/src/gdata gdata
rm -fr gdata-$VERSION
rm gdata-$VERSION.tar.gz