gae-mashapps
============

This code is far from release ready, however it is based on the code referred to
in the blog posting below, and since a couple of readers requested access I have
made a de-personalized version of the code available, more for reading as a basis
for other's development rather than direct use.

When I have some time I will try and finish the code off a bit more and package 
it up properly, with tests etc., in the mean time, appologies if it doesnt work!

Ed Crewe - May 30, 2011 

Using App Engine and Google Apps as a CMS
-----------------------------------------

http://edcrewe.blogspot.com/2011/05/using-app-engine-and-google-apps-as-cms.html


I recently submitted a conference talk, and the form asked for the usual home page url. I tend to use my blog, but I also have stuff scattered around in other places and it made me realize it was probably a good idea to stick this stuff together somewhere. Given that I am not Ryan Giggs it is unlikely that this site would attract a vast amount of public interest, so what I wanted was a fairly simple CMS, that was free for low traffic, and could pull in content and documents from other sites or systems.

For the standard page, image and file CMS functionality then there are now a number of free Wikis / simple CMS, such as <a href="http://www.wikia.com/go">Wikia</a> or <a href="http://sites.google.com/">Google Sites</a>. These tend to have sufficient features, usually no workflow, but often content versioning, which is a bonus. So why not just use one of these out of the box, well where's the fun in that? ... but more seriously they don't cater for full design freedom. They often have fixed templates, they also tend to have very limited, if any, custom content types.

I needed a project type for populating with a portfolio of past work, with image fields and categorisation lists.&nbsp;I also want to pull in some of my documents, and drawing and painting is a hobby of mine, so I fancied adding some of my efforts via a photo sharing site.

Finally there was this blog, well currently its in Wordpress hosted at my workplace, but I may not be there forever, so I thought it was time to migrate to an external service - and have the advantage of some extra tools and integration capabilities as well.

Although happy to do some development, given that this site was a spare time task, I didn't want to spend days on integration code. The leading choice that combines all these tools with a standard well documented API is Google Apps - I plumped for that. But the same principles can be applied to any mainstream hosted content service, since they should all have an API if they are any good. For example there is a <a href="http://code.google.com/p/flickrpy/">python library for Flickr</a>.

So to start mashing up all this Google-ness, I created a site in App Engine's adapted Django which holds the aggregation code for the site, and caters for full customisation and complex content types, such as the portfolio projects.

With inspiration from Django's flatpages app I added a middleware handler to check 404's against requests to the site, any content not found in App Engine falls back to Google Sites and retrieves the page from there via the <a href="http://code.google.com/apis/gdata/">gdata API</a>. So in effect you have an admin interface for editing which is the bare untemplated Google Site, and the public interface for display via App Engine.
Google Sites has the advantage of already being integrated via common authorisation with Google docs, &nbsp;Picasa, YouTube etc. so dropping in slides shows of photos, or Office documents is trivial.&nbsp;

The gdata API is ATOM based and extensive with a full <a href="http://code.google.com/p/gdata-python-client/">python library</a> wrapper. It caters for read or writes of content, ACL, revisions, templates etc. for the various component Apps. Alongside it is the&nbsp;<a href="http://code.google.com/apis/accounts/docs/OAuth2.html">OAuth protocol</a> acting as the single sign on system to bind together the authentication and authorisation. &nbsp;It raises the potential of a developing much larger scale CMS by developing tools to automate site creation and integration from Google App domains of thousands of users.

But back to my home site. For my limited requirements the job was relatively trivial, so I would recommend the Google approach for simple low traffic CMS. &nbsp;But before this becomes a pure advert for Google, I did come across a few random gripes along the way.
