from gdata import service
import datetime
BLOGGER = 'http://www.blogger.com/feeds'

class Blogger:
    """ Uses Google's data API to retrieve the content of a Blogger blog
        to a dummy model - note if site entity's content is not wrapped in 
        valid html tags then it is not available in the feed
    """

    def __init__(self, blog_id='6603837339236629698'):
        """ Set site for content retrieval """
        self.blogger = service.GDataService()
        self.blog_id = blog_id

    def latest_posts(self, months = 9, limit = 5):
        """ Get posts in the last few months up to a limit number
            Each post has the following data properties ...  
            ['author', 'category', 'content', 'contributor', 'control', 
            'extension_attributes', 'extension_elements', 'id', 'link', 
            'published', 'rights', 'source', 'summary', 
            'text', 'title', 'updated']
        """
        query = service.Query()
        query.feed = '%s/%s/posts/default' % (BLOGGER, self.blog_id)
        delta = datetime.timedelta(-months*365/12)
        query.published_min = (datetime.date.today() + delta).isoformat()
        feed = self.blogger.Get(query.ToUri())
        return feed.entry[:limit]
