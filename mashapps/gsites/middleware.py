from mashapps.gsites.views import gsite
from django.http import Http404
from django.conf import settings

class GSiteFallbackMiddleware(object):
    """ Checks for Google Sites page for non-404 responses."""

    def process_response(self, request, response):
        if response: 
            status = getattr(response, 'status_code', 404)
            if status != 404:
                return response 
        try:
            return gsite(request, request.path_info)
        # Return the original response if any errors happened. Because this
        # is a middleware, we can't assume the errors will be caught elsewhere.
        except Http404:
            return response
        except:
            if settings.DEBUG:
                raise
            return response
