import gdata.sites.client 
from google.appengine.ext import db
from datetime import datetime
import re
from django.utils.encoding import force_unicode
killtags = re.compile(r'<[^>]*?>')

try:
    from mashapps.google_config import *
    config = SITE and USER and PW
    if not SOURCE:
        SOURCE = '%s-v1' % SITE
except:
    config = False
if not config: 
    print '''Please add a google_config.py file to this directory with the following strings:
             SITE, USER and PW based on demo_config.py
             These should be the name of the site in Google you are importing to, 
             your Google credentials, and the path to the plone export structure file'''
    exit

def grab_content(response):
    """ Custom converter to just grab the HTML content 
        because the standard entity.content.html loses markup 
    """
    xml = response.read()
    xhtml = ''
    parts = xml.split("<content type='xhtml'>")
    if len(parts)>1:
        parts = parts[1].split("</content>")
        if len(parts)>1:
            xhtml = parts[0]
    return xhtml

class SitesEntity(db.Model):
    """ Dummy wrapper for Google site pages - 
        its never actually saved to the db - since its already in there!
    """
    sites_key = db.StringProperty(required=True)
    id = db.StringProperty(required=True)
    sites_kind = db.StringProperty(required=True)
    revision = db.IntegerProperty(required=True)
    updated = db.DateTimeProperty(required=True)
    description = db.TextProperty()
    html = db.TextProperty()
    url = db.StringProperty()
    feed_uri = db.StringProperty()

class GSite:
    """ Uses Google's data API to retrieve the content of a Google site
        to a dummy model - note if site entity's content is not wrapped in 
        valid html tags then it is not available in the feed
    """

    def __init__(self):
        """ Set site for content retrieval """
        self.client = gdata.sites.client.SitesClient(source=SOURCE, site=SITE)
        self.client.ssl = True  
        self.client.ClientLogin(USER, PW, self.client.auth_service)
        self.rootlen =  len('https://sites.google.com/site/%s' % SITE) 

    def get_page(self, path):
        """ Pass the url to get the pages 
            A single page with no attachments will just return a list of one
        """
        uri = '%s?path=%s' % (self.client.MakeContentFeedUri(), path)
        feed = self.client.GetContentFeed(uri=uri)
        if feed:
            entities = self.get_entities(feed, uri, True)
            if entities:
                return entities[0]
        return None

    def get_entities(self, feed, uri, parent = False):
        """ Retrieve the feed as a list of SitesEntities
        """
        entities = []
        for obj in getattr(feed, 'entry', []):
            if obj:
                if obj.page_name:
                    id = obj.page_name.text
                else:
                    id = obj.title.text
                id = id.encode('ascii','ignore')
                entities.append(self.export_object(uri, id, obj, 
                                                   obj.Kind(), parent))
        return entities

    def export_object(self, uri, id, obj, objtype, parent=False):
        """ Create a SiteEntity from the feed entry 
            Binaries should just link via url
            Other types may have html content
            Populated folder like objects have a feed_uri and children entities
            (NB: filecabinet, webpage, announcementpage)

            TODO: Patch gdata to return obj.content.html correctly!

        """
        text = ''
        feed_uri = ''        
        treatas = 'text'
        children = []
        if objtype == 'attachment':
            treatas = 'binary'
        else:
            if objtype in ['filecabinet','announcementpage']:
                treatas = 'folder'
            if parent and obj.feed_link:
                feed = self.client.GetContentFeed(uri=obj.feed_link.href)
                if len(feed.entry) > 0:
                    feed_uri = obj.feed_link.href
                    children = self.get_entities(feed, feed_uri)
                    treatas = 'folder'

        html = self.client.GetContentFeed(uri=uri,
                                          converter=grab_content)
        if not html:
            try:
                html = obj.content.html
            except:
                html = ''
        if html:
            html = force_unicode(html)
        try:
            descrip = self._get_attr_value(obj)
            descrip = killtags.sub('', force_unicode(descrip))
        except:
            descrip = ''

        if treatas == 'text':
            if html:
                html = html.replace('<html:','<')
                html = html.replace('/html:','/')
                html = '\n\n %s' % html.replace("\r", "")
        revision = 1
        try:
            revision = int(obj.revision.text)
        except:
            pass
        updated = datetime.now()
        try:
            updated = datetime.strptime(obj.updated.text,'%Y-%m-%dT%H:%M:%S.%fZ')
        except:
            pass
        entity = SitesEntity(sites_key = obj.GetNodeId(),
                           id = id, 
                           sites_kind = objtype,
                           revision = revision,
                           updated = updated,
                           description = descrip,
                           url = self._get_web_path(obj),
                           html = html,
                           feed_uri = feed_uri
                           )
        if children:
            setattr(entity,'children',children)
        return entity

    def _get_attr_value(self, obj, attr='summary'):
        """ Attrs can have text or html so check both """
        if hasattr(obj, attr):
            for attr in ('text','html'):
                value = getattr(obj.content, attr, None)
            if value:
                return value
        return ''

    def _get_web_path(self, obj):
        """ Google stores everything in BigTable against hash getNodeId
            hence the web path is not represented via the atom feed content
            Note: links are returned with spaces removed so must remove them 
                  from the id
        """
        try:
            # Gives folder based path whilst GetParentLink is the node id
            link = obj.GetAlternateLink().href
        except:
            try:
                link = obj.GetLink().href
            except:
                link = ''
        if link:
            link = link[self.rootlen:]
            if not link:
                link = '/'
            return link
        return '/'
