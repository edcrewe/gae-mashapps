from django.conf.urls.defaults import *

urlpatterns = patterns('mashapps.gsites.views',
    (r'^(?P<url>.*)$', 'gsite'),
)
