from mashapps.gsites.models import GSite
from django.template import loader, RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from mashapps.gsites.models import GSite
from django.views.decorators.cache import cache_page
from django.shortcuts import render_to_response
from django.http import Http404
from mashapps.utils import toplinks
DEFAULT_TEMPLATE = 'gsites_default.html'
GSITE = 'https://sites.google.com/site/(your google site)'

# This view is called from GSiteFallbackMiddleware.process_response
# when a 404 is raised.

@cache_page(600)
def gsite(request, url):
    """
    Public interface to the google sites page view.
    """
    edit = False
    if not url.startswith('/'):
        url = "/" + url
    if url.endswith('/edit'):
        url = url[:-5]
        edit = True
    page = GSite().get_page(url)
    if page:
        if edit:
            return HttpResponseRedirect('%s%s' % (GSITE, url))
        else:
            props = {'page':page, 
                     'toplinks':toplinks(href=url)}
            return render_to_response(DEFAULT_TEMPLATE, props)
    else:
        raise Http404
