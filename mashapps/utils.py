
LINKS = [["/",'home',True],
         ["http://(your blog).blogspot.com/",'blog', False],
         ["/released-software/",'open source', False],
         ["/project/",'portfolio', False],
         ["/art/",'art', False],
         ["/presentation/",'talks', False],
         ["/cv/",'resume', False]]

def toplinks(href):
    links = LINKS
    parts = href.split('/')
    for part in parts:
        if part:
            href = str('/%s/' % part)
            break
    for link in links:
        link[2] = False
        if link[0] == href:
            link[2] = True
    return links

