from django.conf.urls.defaults import *
from mashapps import settings

urlpatterns = patterns('',
    (r'^project/', include('mashapps.project.urls')),
    (r'^$', 'mashapps.views.site'),
    )

if settings.DEBUG:
    from django.views.static import serve
    _media_url = settings.MEDIA_URL
    if _media_url.startswith('/'):
        _media_url = _media_url[1:]
        urlpatterns += patterns('',
                               (r'^%s(?P<path>.*)$' % _media_url,
                               serve,
                               {'document_root': settings.MEDIA_ROOT}))
        del(_media_url, serve)
