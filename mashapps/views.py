import os
from django.http import HttpResponse, HttpResponseRedirect
from mashapps.project import models
from mashapps.project.forms import *
from django.shortcuts import render_to_response
from mashapps.settings import *
from datetime import datetime
# from google.appengine.api import memcache
from django.views.decorators.cache import cache_page
# from google.appengine.api import users
from mashapps.gsites.models import GSite
from mashapps.blogger.models import Blogger
from mashapps.utils import toplinks

@cache_page(3600)
def get_home(request):
    """ Checks the cache to for home page or sets it - 
        time = 1 hour by default """
    page = GSite().get_page('/home')
    try:
        posts = Blogger().latest_posts()
    except:
        posts = []
    props = {'page':page,
             'blog':posts,
             'toplinks':toplinks(href='/')}
    if page:
        return render_to_response('index.html', props)

def site(request, template='index.html'):
    """ Used for the home page """
    #if template == 'index.html':
    return get_home(request)
