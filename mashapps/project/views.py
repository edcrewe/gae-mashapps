from google.appengine.api import users
from google.appengine.ext import db
import os
from django.http import HttpResponse, HttpResponseRedirect
from mashapps.project import models
from mashapps.project.forms import *
from django.shortcuts import render_to_response
from mashapps.settings import *
from datetime import datetime
# from google.appengine.api import memcache
from django.views.decorators.cache import cache_page

from django.forms.models import model_to_dict
from mashapps.utils import toplinks
from google.appengine.api import images

@cache_page(600)
def get_home(request):
    """ Checks the cache to for home page or sets it - time = 10 mins by default """
    projects = models.Project.all().order('-start_year')
    props = {'projects':projects,
             'picasaname':'add your picasa name',
             'toplinks':toplinks(href='/project/')}
    return render_to_response('project_index.html', props)

@cache_page(600)
def index(request, template='project_index.html', pkey=None):
    """ Used for listing projects on the home page, 
        vote index and results index pages """
    if pkey:
        try:
            user = users.get_current_user()
            project = models.Project.get(pkey)
            props = {'user': user,
                     'picasaname':'add your picasa name',
                     'project': project,
                     'toplinks': toplinks(href='/project/')}
            return render_to_response(template, props)
        except:
            pass
    return get_home(request)

def create(request, pkey=None):
    """ Create a project form """
    user = users.get_current_user()
    props = {'user': user,
             'pkey': pkey,
             'toplinks': toplinks(href='/project/')}
    instance = None
    if user:
        login = None
        logout = ("Welcome, %s! (<a href=\"%s\">sign out</a>)" %
                   (user.nickname(), users.create_logout_url("/")))
        if pkey:
            instance = models.Project.get(pkey)            
    else:
        login = ("<a href=\"%s\">Sign in or register</a>." %
                    users.create_login_url("/create/"))
        logout = None
    # New project form
    if request.method == 'GET':
        projectform = ProjectForm(instance=instance)
    # Write posted project
    if request.method == 'POST':
        projectform = ProjectForm(request.POST, instance=instance)
        if projectform.is_valid():
            project = projectform.save()
            if request.POST.get('createproject',''):
                project.created_user = user
                for fh in ['logo','sample']:
                    if request.FILES.has_key(fh):
                        filedata = request.FILES[fh].read() 
                        setattr(project, fh, db.Blob(str(filedata)))
                        project.put()
                project.save()
                return HttpResponseRedirect(project.get_absolute_url())
    props['projectform'] = projectform 
    props['login'] = login
    props['logout'] = logout
    return render_to_response('create.html', props)

@cache_page(7200)
def project_image(request, field, pkey):
    """ Render app engine stored image """
    project = models.Project.get(pkey)
    image = getattr(project, field, None)

    if image:
        img = images.Image(image)
    
    if field=='logo':
        if img.height >= img.width:
            width = 80
        else:
            width = 150
    else:
        width = 500
    img.resize(width=width)
    img.im_feeling_lucky()
    try:
        newimg = img.execute_transforms(output_encoding=images.PNG)
    except:
        return HttpResponseRedirect("/media/image_not_found.png")
    #build your response
    response = HttpResponse(newimg)
    response['Content-Type'] = 'image/png'
    # set some reasonable cache headers 
    # unless you want the image pulled on every request
    response['Cache-Control'] = 'max-age=7200'
    return response
