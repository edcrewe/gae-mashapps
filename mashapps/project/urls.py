from django.conf.urls.defaults import *

urlpatterns = patterns('mashapps.project',
    (r'^create/', 'views.create'),
    (r'^image/(?P<pkey>[^\.^/]*)/(?P<field>[a-z]*).png$', 'views.project_image'),   
    (r'^(?P<pkey>[^\.^/]*)/edit$', 'views.create'),
    (r'^(?P<pkey>[^\.^/]*)/*$', 'views.index'),
)




    
