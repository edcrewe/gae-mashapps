from google.appengine.ext import db
from django import forms
from django.utils.safestring import mark_safe

class Project(db.Model):
    """ Software projects """
    title = db.StringProperty()
    description = db.TextProperty()
    cms = db.StringListProperty('Application / CMS', default='')
    framework = db.StringListProperty('Framework') 
    language = db.StringListProperty('Language') 
    database = db.StringListProperty('Database') 
    markup = db.StringListProperty('Markup') 
    config = db.StringListProperty('Config') 
    start_year = db.IntegerProperty(default = 2000)
    end_year = db.IntegerProperty(default = 2011)
    live_url = db.LinkProperty()
    doc_url = db.LinkProperty()
    code_url = db.LinkProperty()
    created_date = db.DateTimeProperty(auto_now_add = 1)
    created_user = db.UserProperty()
    logo = db.BlobProperty()
    sample = db.BlobProperty()

    class Meta:
        ordering = ('-start_year','title')
    
    def __str__(self):
        return '%s' % self.title
    
    def get_absolute_url(self):
        return mark_safe('/project/%s' % self.key())


