from django import forms
import models
from django.utils.safestring import mark_safe
from itertools import chain
from google.appengine.ext.db import djangoforms
dd = {'CMS':[u'Google App Engine',u'Google Apps',u'Plone',u'Drupal',u'Moodle',
             u'Mahara',u'Open Commerce System',u'Open Journal Systems',u'uPortal'],
      'FRAMEWORK':[u'Android',u'Django',u'Zope',u'JQuery',u'.NET'],
      'LANGUAGE':[u'Java',u'Python',u'Perl',u'PHP',u'Javascript',u'VB / ASP'],
      'DATABASE':[u'Oracle',u'MS SQL server',u'Postgres',u'MySQL',u'Google Big Table',u'ZODB',u'MongoDB'],
      'MARKUP':[u'HTML',u'RDF',u'XML',u'SOAP',u'OAI:PMH',u'Atom',u'RSS',u'CSS',u'SVG'],
      'CONFIG':[u'BCfg2',u'Fabric',u'Buildout',u'Ant',u'Jenkins',u'Python',u'Shell']
      }

for key, values in dd.items():
    dd[key] = [(value, value) for value in values]

class cbSelectMultiple(forms.widgets.CheckboxSelectMultiple):
    def render(self, name, value, attrs=None, choices=()):
        if value is None: value = []
        has_id = attrs and 'id' in attrs
        final_attrs = self.build_attrs(attrs, name=name)
        output = [u'<ul>']
        # Normalize to strings
        if isinstance(value, basestring):
            value = [value,]
        str_values = set(value) #set([str(v) for v in value])
        for i, (option_value, option_label) in enumerate(chain(self.choices, choices)):
            # If an ID attribute was given, add a numeric index as a suffix,
            # so that the checkboxes don't all have the same ID attribute.
            if has_id:
                final_attrs = dict(final_attrs, id='%s_%s' % (attrs['id'], i))
                label_for = u' for="%s"' % final_attrs['id']
            else:
                label_for = ''
            cb = forms.widgets.CheckboxInput(final_attrs, check_test=lambda value: value in str_values)
            option_value = str(option_value)
            rendered_cb = cb.render(name, option_value)
            #option_label = conditional_escape(force_unicode(option_label))
            output.append(u'<li><label%s>%s %s</label></li>' % (label_for, rendered_cb, option_label))
        output.append(u'</ul>')
        return mark_safe(u'\n'.join(output))


class ProjectForm(djangoforms.ModelForm):
    cms = forms.MultipleChoiceField(choices=dd['CMS'], widget=cbSelectMultiple, required=False)
    framework = forms.MultipleChoiceField(choices=dd['FRAMEWORK'], widget=cbSelectMultiple, required=False)
    language = forms.MultipleChoiceField(choices=dd['LANGUAGE'], widget=cbSelectMultiple, required=False)
    database = forms.MultipleChoiceField(choices=dd['DATABASE'], widget=cbSelectMultiple, required=False)
    markup = forms.MultipleChoiceField(choices=dd['MARKUP'], widget=cbSelectMultiple, required=False)
    config = forms.MultipleChoiceField(choices=dd['CONFIG'], widget=cbSelectMultiple, required=False)

    class Meta:
        model = models.Project
        exclude = ['created_user','created_date']
